# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models 


# Create your models here.
class categorie(models.Model):
    cat_name = models.CharField(max_length=20)
    # image = models.ImageField(upload_to="/category/image/")
    def __str__(self):
      return self.cat_name
class product(models.Model):
    cat_name = models.ForeignKey(categorie,on_delete=models.CASCADE)
    pro_name = models.CharField(max_length=20)
    def __str__(self):
      return self.pro_name
class role(models.Model):
    role = models.CharField(max_length=30)
    def __str__(self):
      return self.role
class employee(models.Model):
    first_name = models.CharField(max_length=20)
    lastname_name = models.CharField(max_length=20)
    username = models.CharField(max_length=20)
    email = models.EmailField(blank=False , max_length=254)
    phone = models.TextField(max_length=20)
    role = models.OneToOneField(role,on_delete=models.CASCADE, primary_key=True)
    created_date =  models.DateTimeField(auto_now=True, blank=True)
    def __str__(self):
      return self.username
class customer(models.Model):
    first_name = models.CharField(max_length=20)
    lastname_name = models.CharField(max_length=20)
    phone = models.TextField(max_length=20)
    def __str__(self):
      return "%s %s" % (self.first_name, self.lastname_name)
class order(models.Model):
    emp_username = models.ForeignKey(employee,on_delete=models.CASCADE)
    customer_username = models.ForeignKey(customer,on_delete=models.CASCADE)
    pro_name = models.ManyToManyField(product)

    def __int__(self):
        return self.customer_username

class order_details(models.Model):
    order = models.OneToOneField(order, on_delete=models.CASCADE)
    qty = models.IntegerField()
    price = models.FloatField()
    created_date =  models.DateTimeField(auto_now=False,auto_now_add=True)
    
    def __int__(self):
        return self.order
    def total(self):
        total = self.price*self.qty 
        return total


    

     



