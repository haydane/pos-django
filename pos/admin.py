# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from .models import categorie, product, role, employee, customer, order, order_details

from django.contrib import admin

# Register your models here.
admin.site.register(categorie)
admin.site.register(product)
admin.site.register(role)
admin.site.register(employee)
admin.site.register(customer)
admin.site.register(order)
admin.site.register(order_details)